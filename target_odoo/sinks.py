from __future__ import annotations

import xmlrpc.client
from datetime import datetime
from typing import Any, Dict, List, Optional

from singer_sdk.plugin_base import PluginBase
from singer_sdk.sinks import RecordSink


class OdooSink(RecordSink):
    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)

        self.url = self.config.get("url")
        self.db = self.config.get("db")
        self.user = self.config.get("username")
        self.password = self.config.get("password")
        self.uid = self.auth()
        self.so_id = {}
        self.models = None

    def auth(self):
        common = xmlrpc.client.ServerProxy("{}/xmlrpc/2/common".format(self.url))
        return common.authenticate(self.db, self.user, str(self.password), {})

    def query_odoo(self, stream_name, filters):
        return self.models.execute_kw(
            self.db, self.uid, str(self.password), stream_name, "search_read", filters
        )

    def find_parnter(self, parnter_name):
        filters = [[["name", "=", parnter_name]]]
        return self.query_odoo("res.partner", filters)

    def find_product(self, product_name):
        filters = [[["name", "=", product_name]]]
        return self.query_odoo("product.product", filters)

    def map_purchase_order(self, record):
        record_processed = {}
        partner = self.find_parnter(record["supplierName"])
        if len(partner) > 0:
            record_processed["partner_id"] = partner[0]["id"]
            due_date = datetime.fromisoformat(record["dueDate"]).strftime("%Y-%m-%d")
            create_date = datetime.fromisoformat(record["createdAt"]).strftime(
                "%Y-%m-%d"
            )
            record_processed["create_date"] = due_date
            record_processed["date_order"] = create_date
        return record_processed

    def process_record(self, record: dict, context: dict) -> None:

        if self.uid == None:
            self.uid = self.auth()
        password = str(self._config.get("password"))
        db = self._config.get("db")

        models = xmlrpc.client.ServerProxy(f"{self.url}/xmlrpc/2/object")
        self.models = models
        stream_name = self.stream_name
        if stream_name == "PurchaseInvoices":
            stream_name = "purchase.order"
        # Getting valid fields
        valid_fields = models.execute_kw(
            db,
            self.uid,
            password,
            stream_name,
            "fields_get",
            [],
            {"attributes": ["string"]},
        )

        record_processed = {}
        if stream_name == "purchase.order":
            record_processed = self.map_purchase_order(record)
        else:
            # Filtering out unvalid records
            for key, value in record.items():
                if value:
                    if key in valid_fields:
                        if key.endswith("_id"):
                            record_processed[key] = int(value)
                        else:
                            record_processed[key] = value

        id = models.execute_kw(
            db, self.uid, str(password), stream_name, "create", [record_processed]
        )
        order_id = id

        if stream_name == "sale.order":
            record_line = record.get("order_lines")
            if record_line:
                for rec in eval(record.get("order_lines")):
                    rec["order_id"] = order_id
                    if rec.get("product_uom_qty"):
                        rec["product_uom_qty"] = int(rec["product_uom_qty"])

                    for key, val in rec.items():
                        if key.endswith("_id"):
                            rec[key] = int(val)
                        else:
                            rec[key] = val
                    try:
                        idl = models.execute_kw(
                            db,
                            self.uid,
                            str(password),
                            f"{stream_name}.line",
                            "create",
                            [rec],
                        )
                    except xmlrpc.client.Fault as error:
                        self.logger.warning(error.faultString)

        if stream_name == "purchase.order":
            record_line = record.get("lineItems")
            if record_line:
                for rec in eval(record.get("lineItems")):
                    line_rec = {}
                    line_rec["order_id"] = order_id
                    product = self.find_product(rec["productName"])
                    if len(product) > 0:
                        product = product[0]
                        line_rec["product_id"] = product["id"]
                        line_rec["name"] = product["name"]
                        line_rec["price_unit"] = rec["unitPrice"]
                        line_rec["product_qty"] = rec["quantity"]
                        line_rec["price_total"] = rec["totalPrice"]
                        if rec.get("product_uom_qty"):
                            line_rec["product_uom_qty"] = int(rec["product_uom_qty"])

                        try:
                            idl = models.execute_kw(
                                db,
                                self.uid,
                                str(password),
                                f"{stream_name}.line",
                                "create",
                                [line_rec],
                            )
                            print(idl)
                        except xmlrpc.client.Fault as error:
                            self.logger.warning(error.faultString)
