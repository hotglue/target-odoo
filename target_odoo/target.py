from __future__ import annotations

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_odoo.sinks import OdooSink


class Targetodoo(Target):
    """Sample target for odoo."""

    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(
            config=config,
            parse_env_config=parse_env_config,
            validate_config=validate_config,
        )

    name = "target-odoo"
    config_jsonschema = th.PropertiesList(
        th.Property("db", th.StringType, required=True),
        th.Property("url", th.StringType, required=True),
        th.Property("username", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
    ).to_dict()
    default_sink_class = OdooSink


if __name__ == "__main__":
    Targetodoo.cli()
